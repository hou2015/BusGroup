package com.wxbus.service.Impl;

import com.github.pagehelper.PageHelper;
import com.wxbus.dao.PassengerMapper;
import com.wxbus.daomain.Passenger;
import com.wxbus.daomain.PassengerExample;
import com.wxbus.service.PassengerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class PassengerServiceImpl implements PassengerService {
    private Log log= LogFactory.getLog(PassengerServiceImpl.class);
    @Resource
    private PassengerMapper passengerMapper;
    @Override
    public List<Passenger> findPassengerList(Integer startNum,Integer num) {
        log.info("查询所有的乘客");
        PageHelper.startPage(startNum,num);
        PassengerExample passengerExample=new PassengerExample();
        List<Passenger> passengerList=passengerMapper.selectByExample(passengerExample);
        return passengerList;
    }

    @Override
    public Passenger findPassengerById(Integer id) {
        return passengerMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updatePassengerStatus(Passenger passenger) {

        log.info("更新乘客信息");
        passengerMapper.updateByPrimaryKeySelective(passenger);

    }

    @Override
    public boolean isIlleagelPassenger(String passengerCitizenship) {
        PassengerExample passengerExample=new PassengerExample();
        passengerExample.createCriteria().andPassengerCitizenshipEqualTo(passengerCitizenship);
        List<Passenger> passengerList=passengerMapper.selectByExample(passengerExample);
        if(passengerList.size()<=0){
            return  false;
        }
        return  true;
    }

    @Override
    public Integer findMainKey(String passengerCitizenship) {
        PassengerExample passengerExample=new PassengerExample();
        passengerExample.createCriteria().andPassengerCitizenshipEqualTo(passengerCitizenship);
        List<Passenger> passengerList=passengerMapper.selectByExample(passengerExample);
        if(passengerList.size()<=0){
            return  0;
        }
        return passengerList.get(0).getId();
    }
}
