$(document).ready(function () {

    //没有更多数据
    var noneleft = "<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing = "<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    $('div.content.table-responsive.table-full-width').empty();
    var body={startNum:1,num:500};
    $.ajax({
        type: 'POST',
        url: '/web/search/passenger',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var more="<div class='panel panel-success more-flag0'>"+
                "<div class='panel-heading'>" +
                "<center>" +
                "<h3 class='panel-title'>点击加载更多数据</h3>" +
                "</center>" +
                "</div>" +
                "</div>";
            var theader="<table class='table table-striped'>" +
                "<thead>" +
                "<th>昵称</th>" +
                "<th>真实姓名</th>" +
                "<th>性别</th>" +
                "<th>身份证号</th>" +
                "<th>当前状态</th>" +
                "<th>冻结次数</th>" +
                "<th>操作</th>"+
                "</thead>" +
                "<tbody>" +
                "</tbody>" +
                "</table>";
            $('div.content.table-responsive.table-full-width').html(theader);
            var tbody="";
            $(res.data).each(function (index,item) {
                if(item.passengerStatus==0){
                    tbody+="<tr id='"+item.id+"'>" +
                        "<td>"+item.passengerNickname+"</td>" +
                        "<td>"+item.passengerName+"</td>" +
                        "<td>"+item.passengerGender+"</td>" +
                        "<td>"+item.passengerCitizenship+"</td>" +
                        "<td>可用</td>" +
                        "<td>"+item.deleted+"</td>" +
                        "<td><button class='btn btn-primary' data-pid='"+item.id+"' data-status='1'>冻结</button><button class='btn disabled' data-pid='"+item.id+"' data-status='0'>解除冻结</button></td>" +
                        "</tr>";
                }
                if(item.passengerStatus==1){
                    tbody+="<tr id='"+item.id+"'>" +
                        "<td>"+item.passengerNickname+"</td>" +
                        "<td>"+item.passengerName+"</td>" +
                        "<td>"+item.passengerGender+"</td>" +
                        "<td>"+item.passengerCitizenship+"</td>" +
                        "<td>禁用</td>" +
                        "<td>"+item.deleted+"</td>" +
                        "<td><button class='btn disabled' data-pid='"+item.id+"' data-status='1'>冻结</button><button class='btn btn-primary' data-pid='"+item.id+"' data-status='0'>解除冻结</button></td>" +
                        "</tr>";
                }

            })
            $('tbody').append(tbody);
            if(res.data.length<10){
                $('div.content.table-responsive.table-full-width').append(noneleft);
            }else{
                $('div.content.table-responsive.table-full-width').append(more);
            }

        }
    })


    //管理员信息点击加载更多
    $(document).on("click","div.panel.panel-success.more-flag0",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/passenger',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    if(item.passengerStatus==0){
                        tbody+="<tr id='"+item.id+"'>" +
                            "<td>"+item.passengerNickname+"</td>" +
                            "<td>"+item.passengerName+"</td>" +
                            "<td>"+item.passengerGender+"</td>" +
                            "<td>"+item.passengerCitizenship+"</td>" +
                            "<td>可用</td>" +
                            "<td>"+item.deleted+"</td>" +
                            "<td><button class='btn btn-primary' data-pid='"+item.id+"' data-status='1'>冻结</button><button class='btn disabled' data-pid='"+item.id+"' data-status='0'>解除冻结</button></td>" +
                            "</tr>";
                    }
                    if(item.passengerStatus==1){
                        tbody+="<tr id='"+item.id+"'>" +
                            "<td>"+item.passengerNickname+"</td>" +
                            "<td>"+item.passengerName+"</td>" +
                            "<td>"+item.passengerGender+"</td>" +
                            "<td>"+item.passengerCitizenship+"</td>" +
                            "<td>禁用</td>" +
                            "<td>"+item.deleted+"</td>" +
                            "<td><button class='btn disabled' data-pid='"+item.id+"' data-status='1'>冻结</button><button class='btn btn-primary' data-pid='"+item.id+"' data-status='0'>解除冻结</button></td>" +
                            "</tr>";
                    }

                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    })
    
    $(document).on("click","button.btn.btn-primary",function () {
        var Passenger={};
        Passenger.id=$(this).data("pid");
        Passenger.passengerStatus=$(this).data("status");
        $.ajax({
            type: 'POST',
            url: '/web/update/passenger',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(Passenger),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                window.location.reload();
            }
        })
    })
})