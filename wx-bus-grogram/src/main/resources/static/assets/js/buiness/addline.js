$(document).ready(function () {
    var body={startNum:1,num:500};
    //查找所有已经有的站点
    $.ajax({
        type: 'POST',
        url: '/web/search/searchusingstation',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var dropList="";
            $(res.data).each(function (index,item) {
                dropList+="<li class='selected' data-stationid='"+item.stationId+"' data-position='"+item.stationCoord+"'><a href='#'>"+item.stationName+"</a></li>";
            })
            $("ul.dropdown-menu.pull-right.station").append(dropList);

        }
    })
    //查找可用的运行周期
    $.ajax({
        type: 'POST',
        url: '/web/search/runtime',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var dropList="";
            $(res.data).each(function (index,item) {
                dropList+="<li class='selected' ><a href='#'>"+item.routeinquiryperiod+"</a></li>";
            })
            $("ul.dropdown-menu.pull-right.runtime").append(dropList);

        }
    })
    
    $(document).on("click","li.selected",function () {
        var stationId="";
        if($(this).parent("ul").parent("div").parent("div").prev("label").text()=="途径站"){
            if($("input[name='stationId']").val()==""){
                $("input[name='stationId']").val($(this).text());
                stationId= $(this).data("stationid");
                $("input[name='stationId']").data("stationid",stationId);
                $(this).remove();
            }else{
                $("input[name='stationId']").val($("input[name='stationId']").val()+"、"+$(this).text());
                stationId=$(this).parent("ul").parent("div").prev("input").data("stationid")+","+$(this).data("stationid");
                $("input[name='stationId']").data("stationid",stationId);
                $(this).remove();
            }
        }else{

            $(this).parent("ul").parent("div").prev("input").data("position",$(this).data("position"));
            $(this).parent("ul").parent("div").prev("input").val($(this).text());
        }

    })

    $(document).on("click","button.btn.btn-info.btn-fill.btn-wd",function () {
        var Route = {};
        Route.startSite = $("input[name='startSite']").val();
        Route.startCoord=$("input[name='startSite']").data("position");
        if (Route.startSite == "") {
            alert("请选择起始站!")
            return false;
        }


        Route.endSite = $("input[name='endSite']").val();
        Route.endCoord=$("input[name='endSite']").data("position");
        if (Route.endSite == "") {
            alert("请选择终点站!")
            return false;
        }
        Route.stationId = $("input[name='stationId']").data("stationid");
        Route.startTime = $("input[name='startTime']").val();
        if (Route.startTime == "") {
            alert("请选择出发时间!")
            return false;
        }

        Route.endTime = $("input[name='endTime']").val();
        if (Route.endTime == "") {
            alert("请选择到达时间!")
            return false;
        }

        Route.runTime = $("input[name='runTime']").val();
        if (Route.runTime == "") {
            alert("请选择运行周期!")
            return false;
        }
        Route.startRecruit = $("input[name='startRecruit']").val();
        if (Route.startRecruit == "") {
            alert("请选择开始运行时间!")
            return false;
        }
        Route.endsRecruit = $("input[name='endsRecruit']").val();
        if (Route.endsRecruit == "") {
            alert("请选择运行截止时间!")
            return false;
        }
        $.ajax({
            type: 'POST',
            url: '/web/add/addroute',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(Route),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                window.location.href="/web/linemanage";
            }
        })
    })

})