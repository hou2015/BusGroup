$(document).ready(function () {
    //没有更多数据
    var noneleft = "<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing = "<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";


    //初次加载可用站点

    $('div.content.table-responsive.table-full-width').empty();
    var page = 1;
    var body = {startNum: page, num: 10};
    $('input[type=hidden]').attr("value", page);

    $.ajax({
        type: 'POST',
        url: '/web/search/searchusingstation',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            if (res.data.length <= 0) {
                $('div.content.table-responsive.table-full-width').html(nothing);
            } else {
                var more = "<div class='panel panel-success more-flag0'>" +
                    "<div class='panel-heading'>" +
                    "<center>" +
                    "<h3 class='panel-title'>点击加载更多数据</h3>" +
                    "</center>" +
                    "</div>" +
                    "</div>";
                var thead = "<table class='table table-striped'>" +
                    "<thead>" +
                    "<th>站点编号</th>" +
                    "<th>站点名称</th>" +
                    "<th>站点位置</th>" +
                    "<th>操作</th>" +
                    "</thead>" +
                    "<tbody>" +
                    "</tbody>" +
                    "</table>";
                $('div.content.table-responsive.table-full-width').html(thead);
                var tbody = "";
                var stationStatusStr = "";
                var lng, lat;
                var sitePosition = "";
                $(res.data).each(function (index, item) {

                    sitePosition = item.stationCoord;
                    lng = sitePosition.substring(0, sitePosition.lastIndexOf(','));
                    lat = sitePosition.substring(sitePosition.lastIndexOf(',')+1);
                    tbody += "<tr id='" + item.stationId + "'>" +
                        "<td>" + item.stationId + "</td>" +
                        "<td>" + item.stationName + "</td>" +
                        "<td><button class='btn btn-primary map' data-toggle='modal' data-target='#myModal' data-lng='" + lng + "' data-lat='" + lat + "'>打开地图</button></td>" +

                        "<td><button class='btn btn-primary up' data-routid='"+item.stationId+"' data-status='1'>停用</button></td>" +
                        "</tr>";
                })
                $('tbody').append(tbody);
                if (res.data.length < 10) {
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                } else {
                    $('div.content.table-responsive.table-full-width').append(more);
                }
            }
        }

    });


    //flag_0点击 再次点击可用站点
    $('li.flag_0').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class", "active flag_0");
        $('li.flag_1').attr("class", "flag_1");


        var page = 1;
        var body = {startNum: page, num: 10};
        $('input[type=hidden]').attr("value", page);

        $.ajax({
            type: 'POST',
            url: '/web/search/searchusingstation',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                if (res.data.length <= 0) {
                    $('div.content.table-responsive.table-full-width').html(nothing);
                } else {
                    var more = "<div class='panel panel-success more-flag0'>" +
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var thead = "<table class='table table-striped'>\n" +
                        "<thead>\n" +
                        "<th>站点编号</th>\n" +
                        "<th>站点名称</th>\n" +
                        "<th>站点位置</th>\n" +
                        "<th>操作</th>\n" +
                        "</thead>" +
                        "<tbody>" +
                        "</tbody>" +
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(thead);
                    var tbody = "";

                    var lng, lat;
                    var sitePosition = "";
                    $(res.data).each(function (index, item) {

                        sitePosition = item.stationCoord;
                        lng = sitePosition.substring(0, sitePosition.lastIndexOf(','));
                        lat = sitePosition.substring(sitePosition.lastIndexOf(',')+1);
                        tbody += "<tr id='" + item.stationId + "'>" +
                            "<td>" + item.stationId + "</td>" +
                            "<td>" + item.stationName + "</td>" +
                            "<td><button class='btn btn-primary map' data-toggle='modal' data-target='#myModal' data-lng='" + lng + "' data-lat='" + lat + "'>打开地图</button></td>" +
                            "<td><button class='btn btn-primary up' data-routid='"+item.stationId+"' data-status='1'>停用</button></td>" +
                            "</tr>";
                    })
                    $('tbody').append(tbody);
                    if (res.data.length < 10) {
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    } else {
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        });
    });


    //可用站点加载更多  flag=0
    $(document).on("click", "div.panel.panel-success.more-flag0", function () {
        var page = $('input[type=hidden]').val();
        page = parseInt(page, 10) + 1;
        var body = {startNum: page, num: 10};
        $('input[type=hidden]').attr("value", page);
        $.ajax({
            type: 'POST',
            url: '/web/search/searchusingstation',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                var tbody = "";
                var lng, lat;
                var sitePosition = "";
                $(res.data).each(function (index, item) {

                    sitePosition = item.stationCoord;
                    lng = sitePosition.substring(0, sitePosition.lastIndexOf(','));
                    lat = sitePosition.substring(sitePosition.lastIndexOf(',')+1);
                    tbody += "<tr id='" + item.stationId + "'>" +
                        "<td>" + item.stationId + "</td>" +
                        "<td>" + item.stationName + "</td>" +
                        "<td><button class='btn btn-primary map' data-toggle='modal' data-target='#myModal' data-lng='" + lng + "' data-lat='" + lat + "'>打开地图</button></td>" +
                        "<td><button class='btn btn-primary up' data-routid='"+item.stationId+"' data-status='1'>停用</button></td>" +
                        "</tr>";

                })
                $('tbody').append(tbody);
                if (res.data.length < 10) {
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    });

//  ---------------------------------------------以下为已经停用的站点---------------------------------------------

    //flag_1点击 加载停用站点
    $('li.flag_1').click(function () {
        $('div.content.table-responsive.table-full-width').empty();

        $('li.flag_0').attr("class", "flag_0");
        $('li.flag_1').attr("class", "active  flag_1");


        var page = 1;
        var body = {startNum: page, num: 10};
        $('input[type=hidden]').attr("value", page);

        $.ajax({
            type: 'POST',
            url: '/web/search/searchstopstation',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                if (res.data.length <= 0) {
                    $('div.content.table-responsive.table-full-width').html(nothing);
                } else {
                    var more = "<div class='panel panel-success more-flag1'>" +
                        "<div class='panel-heading'>" +
                        "<center>" +
                        "<h3 class='panel-title'>点击加载更多数据</h3>" +
                        "</center>" +
                        "</div>" +
                        "</div>";
                    var thead = "<table class='table table-striped'>\n" +
                        "<thead>" +
                        "<th>站点编号</th>" +
                        "<th>站点名称</th>" +
                        "<th>站点位置</th>" +
                        "<th>操作</th>" +
                        "</thead>" +
                        "<tbody>" +
                        "</tbody>" +
                        "</table>";
                    $('div.content.table-responsive.table-full-width').html(thead);
                    var tbody = "";
                    var lng, lat;
                    var sitePosition = "";
                    $(res.data).each(function (index, item) {

                        sitePosition = item.stationCoord;
                        lng = sitePosition.substring(0, sitePosition.lastIndexOf(','));
                        lat = sitePosition.substring(sitePosition.lastIndexOf(',')+1);
                        tbody += "<tr id='" + item.stationId + "'>" +
                            "<td>" + item.stationId + "</td>" +
                            "<td>" + item.stationName + "</td>" +
                            "<td><button class='btn btn-primary map' data-toggle='modal' data-target='#myModal' data-lng='" + lng + "' data-lat='" + lat + "'>打开地图</button></td>" +
                            "<td><button class='btn btn-primary up' data-routid='"+item.stationId+"' data-status='0'>启用</button></td>" +
                            "</tr>";
                    })
                    $('tbody').append(tbody);
                    if (res.data.length < 10) {
                        $('div.content.table-responsive.table-full-width').append(noneleft);
                    } else {
                        $('div.content.table-responsive.table-full-width').append(more);
                    }
                }
            }

        });
    });


    //可用站点加载更多  flag=0
    $(document).on("click", "div.panel.panel-success.more-flag1", function () {
        var page = $('input[type=hidden]').val();
        page = parseInt(page, 10) + 1;
        var body = {startNum: page, num: 10};
        $('input[type=hidden]').attr("value", page);
        $.ajax({
            type: 'POST',
            url: '/web/search/searchstopstation',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify(body),
            error: function () {
                alert("加载失败，请刷新重试!");
            },
            success: function (res) {
                var tbody = "";
                var lng, lat;
                var sitePosition = "";
                $(res.data).each(function (index, item) {
                    sitePosition = item.stationCoord;
                    lng = sitePosition.substring(0, sitePosition.lastIndexOf(','));
                    lat = sitePosition.substring(sitePosition.lastIndexOf(',')+1);
                    tbody += "<tr id='" + item.stationId + "'>" +
                        "<td>" + item.stationId + "</td>" +
                        "<td>" + item.stationName + "</td>" +
                        "<td><button class='btn btn-primary map' data-toggle='modal' data-target='#myModal' data-lng='" + lng + "' data-lat='" + lat + "'>打开地图</button></td>" +
                        "<td><button class='btn btn-primary up' data-status='0' data-routid='"+item.stationId+"'>启用</button></td>" +
                        "</tr>";

                })
                $('tbody').append(tbody);
                if (res.data.length < 10) {
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    });


    $(document).on("click", "button.btn.btn-primary.up", function () {
        var jsonObj = {};
        jsonObj.stationStatus = $(this).data("status");
        jsonObj.stationId = $(this).data("routid");

        $.ajax({
            type: 'POST',
            url: '/web/operate/stationoperate',
            contentType: "application/json;charset=utf-8",
            DataType: "json",
            data: JSON.stringify(jsonObj),
            error: function () {
                alert("加载失败，请刷新重试！");
            },
            success: function (res) {
                if (res.errno == 0) {
                    $("tr#"+jsonObj.stationId).remove();
                }
            }
        })
    })
    $(document).on("click", "button.btn.btn-default.cs1",function () {
        window.location.reload();
    })

})