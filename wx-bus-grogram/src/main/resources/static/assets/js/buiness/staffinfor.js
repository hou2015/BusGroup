$(document).ready(function () {

    //没有更多数据
    var noneleft = "<div class='panel panel-success noneleft'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有更多数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    //没有数据
    var nothing = "<div class='panel panel-success nothing'>" +
        "<div class='panel-heading'>" +
        "<center>" +
        "<h3 class='panel-title'>没有数据</h3>" +
        "</center>" +
        "</div>" +
        "</div>";
    $('div.content.table-responsive.table-full-width').empty();
    var body={startNum:1,num:500};
    $.ajax({
        type: 'POST',
        url: '/web/search/findalladmin',
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        data: JSON.stringify(body),
        error: function () {
            alert("加载失败，请刷新重试!");
        },
        success: function (res) {
            var more="<div class='panel panel-success more-flag0'>"+
                "<div class='panel-heading'>" +
                "<center>" +
                "<h3 class='panel-title'>点击加载更多数据</h3>" +
                "</center>" +
                "</div>" +
                "</div>";
            var theader="<table class='table table-striped'>" +
                "<thead>" +
                "<th>账号</th>\n" +
                "<th>所属公司</th>" +
                "<th>职位信息</th>" +
                "<th>操作</th>" +
                "</thead>" +
                "<tbody>" +
                "</tbody>" +
                "</table>";
            $('div.content.table-responsive.table-full-width').html(theader);
            var tbody="";
            var powStr="";
            $(res.data).each(function (index,item) {
                switch (item.adminPower){
                    case 1:powStr="超级管理";break;
                    case 2:powStr="车辆管理";break;
                    case 3:powStr="路线管理";break;
                    case 4:powStr="人事管理";break;
                    case 5:powStr="招募管理";break;
                }
                tbody+="<tr>"+
                    "<td>"+item.adminId+"</td>" +
                    "<td>"+item.adminOwner+"</td>" +
                    "<td>"+powStr+"</td>" +
                    "<td><a class='btn'>权限变更</a></td>" +
                    "</tr>";
            })
            $('tbody').append(tbody);
            if(res.data.length<10){
                $('div.content.table-responsive.table-full-width').append(noneleft);
            }else{
                $('div.content.table-responsive.table-full-width').append(more);
            }

        }
    })


    //管理员信息点击加载更多
    $(document).on("click","div.panel.panel-success.more-flag0",function () {
        var page=$('input[type=hidden]').val();
        page=parseInt(page,10)+1;
        var body={startNum:page,num:10};
        $('input[type=hidden]').attr("value",page);
        $.ajax({
            type:'POST',
            url:'/web/search/findalladmin',
            contentType:"application/json;charset=utf-8",
            dataType:"json",
            data:JSON.stringify(body),
            error:function () {
                alert("加载失败，请刷新重试!");
            },
            success:function (res) {
                var tbody="";
                $(res.data).each(function (index,item) {
                    switch (item.adminPower){
                        case 1:powStr="超级管理";break;
                        case 2:powStr="车辆管理";break;
                        case 3:powStr="路线管理";break;
                        case 4:powStr="人事管理";break;
                        case 5:powStr="招募管理";break;
                    }
                    tbody+="<tr>"+
                        "<td>"+item.adminId+"</td>" +
                        "<td>"+item.adminOwner+"</td>" +
                        "<td>"+powStr+"</td>" +
                        "<td><a class='btn'>权限变更</a></td>" +
                        "</tr>";
                })
                $('tbody').append(tbody);
                if(res.data.length<10){
                    $('div.content.table-responsive.table-full-width div:last-child').remove();
                    $('div.content.table-responsive.table-full-width').append(noneleft);
                }

            }

        });

    });
})